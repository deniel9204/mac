# README #

This README would normally document whatever steps are necessary to get application up and running.

### What is this repository for? ###

Collecting...

### How do I get set up? ###

- DB: PostgreSQL APP: SpringBoot

- setting up DB env > /src/main/resources/config/application.properties
	
- create mac schema from /resources/db/create_schema_and_user.sql

- generate POJO-s from JSON -> gradle generateJsonSchema2Pojo
	
- clean database > gradle flywayClean	

- generate database > gradle flywayMigrate

- generate DATA POJO-s and DAO-s > gradle build

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

akumul@gmail.com