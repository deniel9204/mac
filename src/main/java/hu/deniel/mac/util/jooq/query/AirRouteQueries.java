package hu.deniel.mac.util.jooq.query;

import static hu.deniel.mac.generatedsource.jooq.postgres.db.Tables.AIR_ROUTE;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import org.jooq.DSLContext;
import org.jooq.InsertFinalStep;
import org.jooq.Queries;
import org.jooq.Query;
import org.jooq.UpdateConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.pojos.AirRoute;
import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.records.AirRouteRecord;

@Component
public class AirRouteQueries implements Queries {

	@Autowired
	private DSLContext dsl;

	private Collection<Query> queries;

	public AirRouteQueries() {
		this.queries = new ArrayList<Query>();
	}

	public final void addQuery(AirRoute airRoute) {

		// if update 'WHERE' condition return 0 row, do nothing...
		UpdateConditionStep<AirRouteRecord> updateQ = dsl.update(AIR_ROUTE)
				.set(dsl.newRecord(AIR_ROUTE, beforeUpdate(dsl.newRecord(AIR_ROUTE, airRoute))))
				.where(AIR_ROUTE.AIR_ROUTE_REF.eq(airRoute.getAirRouteRef()))
				.and(AIR_ROUTE.FARE_TYPE.eq(airRoute.getFareType()))
				.and(AIR_ROUTE.CLUB_CARD.eq(airRoute.getClubCard()));
		// if insert is failed because duplicate key.. do nothing
		InsertFinalStep<AirRouteRecord> insertQ = dsl.insertInto(AIR_ROUTE)
				.set(beforeInsert(dsl.newRecord(AIR_ROUTE, airRoute)))
				.onDuplicateKeyIgnore();

		this.queries.add(updateQ);
		this.queries.add(insertQ);
	}

	private AirRouteRecord beforeUpdate(AirRouteRecord airRouteRecord) {
		airRouteRecord.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
		return airRouteRecord;
	}

	private AirRouteRecord beforeInsert(AirRouteRecord airRouteRecord) {
		airRouteRecord.setCreatedTime(new Timestamp(System.currentTimeMillis()));
		airRouteRecord.setCreatedBy("MAC_BATCH");
		airRouteRecord.setLastModifiedTime(new Timestamp(System.currentTimeMillis()));
		return airRouteRecord;
	}

	@Override
	public final Query[] queries() {
		return queries.toArray(new Query[0]);
	}

	public final void clearQueries() {
		this.queries.clear();
	}

}
