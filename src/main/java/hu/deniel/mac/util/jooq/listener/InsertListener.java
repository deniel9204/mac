package hu.deniel.mac.util.jooq.listener;

import org.jooq.RecordContext;
import org.jooq.impl.DefaultRecordListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.records.AirRouteRecord;

public class InsertListener extends DefaultRecordListener {

	private static final Logger logger = LoggerFactory.getLogger(InsertListener.class);

	@Override
	public void insertStart(RecordContext ctx) {
		logger.debug("insertStart");

		if (ctx.record() instanceof AirRouteRecord) {
			logger.debug("AirRouteRecord");
			AirRouteRecord airRoute = (AirRouteRecord) ctx.record();
			airRoute.setAirportFrom("Budapest");
		}
	}
	
	@Override
	public void updateStart(RecordContext ctx) {
		// TODO Auto-generated method stub
		super.updateStart(ctx);
	}
}
