package hu.deniel.mac.util.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackages = "hu.deniel.mac")
@EnableAsync
@EnableScheduling
public class MacApplication {

	public static void main(String[] args) {
		SpringApplication.run(MacApplication.class, args);
	}
}
