package hu.deniel.mac.service;

import java.util.Collection;
import java.util.List;

import org.jooq.Condition;
import org.jooq.Queries;
import org.jooq.SortField;
import org.springframework.transaction.annotation.Transactional;

import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.pojos.AirRoute;

public interface AirRouteService {
	

	@Transactional
	void save(AirRoute airRoute);
	
	@Transactional
	void saveOrUpdate(AirRoute airRoute);

	@Transactional
	AirRoute findById(Integer id);
	
	@Transactional
	void update(AirRoute book);

	List<AirRoute> list(Collection<Condition> conditions, Collection<SortField<?>> fields);
	
	@Transactional
	void saveOrUpdateBatch(Queries queries);


}
