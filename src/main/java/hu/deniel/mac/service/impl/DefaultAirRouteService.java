package hu.deniel.mac.service.impl;

import static hu.deniel.mac.generatedsource.jooq.postgres.db.Tables.AIR_ROUTE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Queries;
import org.jooq.SortField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.daos.AirRouteDao;
import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.pojos.AirRoute;
import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.records.AirRouteRecord;
import hu.deniel.mac.service.AirRouteService;

@Service
public class DefaultAirRouteService implements AirRouteService {

	private static final Logger logger = LoggerFactory.getLogger(DefaultAirRouteService.class);

	@Autowired
	private DSLContext dsl;

	@Autowired
	private AirRouteDao airRouteDao;

	@Override
	@Transactional
	public void save(AirRoute airRoute) {
		airRouteDao.insert(airRoute);
	}

	@Override
	@Transactional
	public void saveOrUpdate(AirRoute airRoute) {
		AirRouteRecord record = dsl.newRecord(AIR_ROUTE, airRoute);
		record.store();
	}

	@Override
	public AirRoute findById(Integer id) {
		return airRouteDao.findById(id);
	}

	/**
	 * Be carefull
	 */
	@Override
	public List<AirRoute> list(Collection<Condition> conditions, Collection<SortField<?>> fields) {

		/**
		 * Check where conditions
		 */
		if (conditions == null) {
			logger.warn("Where conditions is null;");
			throw new IllegalArgumentException("Conditions cannot be null.");
		}

		/**
		 * Check sort fields
		 */
		if (fields == null) {
			logger.warn("Orderby fields is null;");
			fields = new ArrayList<SortField<?>>();
		}

		/**
		 * return query result
		 */
		return dsl.selectFrom(AIR_ROUTE)
				.where(conditions)
				.orderBy(fields)
				.fetchInto(AirRoute.class);
	}

	@Override
	@Transactional
	public void update(AirRoute airRoute) {
		airRouteDao.update(airRoute);
	}

	@Override
	@Transactional
	public void saveOrUpdateBatch(Queries queries) {
		dsl.batch(queries).execute();
	}
	
}
