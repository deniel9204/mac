package hu.deniel.mac.module.wizzair;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.pojos.AirRoute;
import hu.deniel.mac.generatedsource.json2pojo.wizzair.City;
import hu.deniel.mac.generatedsource.json2pojo.wizzair.Fare;
import hu.deniel.mac.generatedsource.json2pojo.wizzair.FlightList;
import hu.deniel.mac.generatedsource.json2pojo.wizzair.OutboundFlight;
import hu.deniel.mac.generatedsource.json2pojo.wizzair.WizzairAirlines;
import hu.deniel.mac.generatedsource.json2pojo.wizzair.WizzairAirports;
import hu.deniel.mac.generatedsource.json2pojo.wizzair.WizzairRequest;
import hu.deniel.mac.module.MacModule;
import hu.deniel.mac.service.AirRouteService;
import hu.deniel.mac.util.jooq.query.AirRouteQueries;

@Component
public class WizzAirModule implements MacModule {

	private static final Logger logger = LoggerFactory.getLogger(WizzAirModule.class);

	private static final String FLIGHT_API = "https://be.wizzair.com/3.3.1/Api/search/search";
	private static final String AIRPORT_API = "https://be.wizzair.com/3.3.1/Api/asset/map?languageCode=en-gb";

	private static final String SOURCE_SYSTEM = "WIZZAIR";
	private static final int DAYS = 210;

	private final List<City> airports = listAirports();

	private Integer recordInserted;

	@Autowired
	private AirRouteService airRouteService;

	@Autowired
	private AirRouteQueries airRouteQueries;

	@Override
	public void runModule() {
		logger.info("WizzAir module started...");

		// INIT section
		recordInserted = 0;
		StopWatch watchAll = new StopWatch();
		watchAll.start();
		StopWatch watchCurrent = new StopWatch();
		Calendar cal = Calendar.getInstance();

		// RUN section
		for (int i = 0; i < DAYS; i++) {
			// RUN INIT section
			cal.add(Calendar.DAY_OF_MONTH, 1);
			watchCurrent.start((i + 1) + " day process runtime.");

			// RUN RUN section
			//@formatter:off
			airports.forEach(airport -> airport.getConnections()
					.forEach(route -> {
							updateRoutes(airport, route.getIata(), cal.getTime());
					}));
			//@formatter:on

			// RUN POST section
			airRouteService.saveOrUpdateBatch(airRouteQueries);
			airRouteQueries.clearQueries();
			watchCurrent.stop();
			logger.info((i + 1) + " day process ended.");
		}

		// POST section
		logger.info(watchCurrent.prettyPrint());
		watchAll.stop();
		logger.info("WizzAir module ended in " + watchAll.getTotalTimeSeconds() + " second and " + recordInserted
				+ " record processed...");
	}

	private void updateRoutes(City airportFrom, String airportTo, Date firstDate) {
		try {
			getAirportRoutes(airportFrom, airportTo, firstDate);
		} catch (RestClientException e) {
			logger.error("Cannot extract response from: " + airportFrom.getIata() + " to: " + airportTo);
		}
	}

	private void getAirportRoutes(City airportFrom, String airportTo, Date firstDate) {
		final HttpEntity<WizzairRequest> request = createWizzAirRequest(airportFrom, airportTo, firstDate);
//		logger.info("Request object: " + new XStream().toXML(request));
		AsyncRestTemplate rTemplate = new AsyncRestTemplate();
		rTemplate.postForEntity(FLIGHT_API, request, WizzairAirlines.class)
				.addCallback(new ListenableFutureCallback<ResponseEntity<WizzairAirlines>>() {
					@Override
					public void onSuccess(ResponseEntity<WizzairAirlines> result) {
//						logger.info("[SUCCEED] Query succeed on from: " + airportFrom.getIata() + " to " + airportTo + " on "
//								+ firstDate);
						saveAirlines(result.getBody());
					}

					@Override
					public void onFailure(Throwable t) {
						// Need assertions
//						logger.error("[FAILED] Query failed on from: " + airportFrom.getIata() + " to " + airportTo + " on "
//								+ firstDate);
					}
				});
//		request = null;
	}

	private void saveAirlines(WizzairAirlines airlines) {
		//@formatter:off
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		for (OutboundFlight flight : airlines.getOutboundFlights()) {
			for (Fare fare : flight.getFares()) {
				AirRoute airRoute = new AirRoute();
				airRoute.setAirportFrom(flight.getDepartureStation());
				airRoute.setAirportTo(flight.getArrivalStation());
				airRoute.setAirRouteRef(flight.getFlightSellKey());
				
				try {
					airRoute.setArrivalTime(new Timestamp(formatter.parse(flight.getArrivalDateTime()).getTime()));
					airRoute.setStartTime(new Timestamp(formatter.parse(flight.getDepartureDateTime()).getTime()));
				} catch (ParseException e1) {
					e1.printStackTrace();
				}

				airRoute.setCostCurrency(fare.getBasePrice().getCurrencyCode());
				airRoute.setCostNumBase(fare.getBasePrice().getAmount());
				airRoute.setCostNumDiscounted(fare.getDiscountedPrice().getAmount());
				
				airRoute.setFareType(fare.getBundle());
				airRoute.setRouteNumber(flight.getFlightNumber());
				airRoute.setSourceSystem(SOURCE_SYSTEM);
				
				airRoute.setClubCard(fare.getWdc());
				
				airRoute.setAirportFromCity(airports.stream()
						.filter(ai -> ai.getIata().equals(flight.getDepartureStation()))
						.findFirst()
						.map(e -> e.getShortName())
						.get());
				airRoute.setAirportToCity(airports.stream()
						.filter(ai -> ai.getIata().equals(flight.getArrivalStation()))
						.findFirst()
						.map(e -> e.getShortName())
						.get());
				
				recordInserted++;
				airRouteQueries.addQuery(airRoute);
//				airRouteService.save(airRoute);
			}
			
		}
		
//		airlines.getOutboundFlights().forEach(flight -> {
//			flight.getFares().forEach(fare -> {
//				AirRoute airRoute = new AirRoute();
//				airRoute.setAirportFrom(flight.getDepartureStation());
//				airRoute.setAirportTo(flight.getArrivalStation());
//				airRoute.setAirRouteRef(flight.getFlightSellKey());
//				airRoute.setArrivalTime(new Timestamp(Date.from(Instant.parse(flight.getArrivalDateTime())).getTime()));
//
//				airRoute.setCostCurrency(fare.getBasePrice().getCurrencyCode());
//				airRoute.setCostNumBase(fare.getBasePrice().getAmount());
//				airRoute.setCostNumDiscounted(fare.getDiscountedPrice().getAmount());
//				
//				airRoute.setFareType(fare.getBundle());
//				airRoute.setRouteNumber(flight.getFlightNumber());
//				airRoute.setSourceSystem(SOURCE_SYSTEM);
//				
//				airRoute.setClubCard(fare.getWdc());
//				
//				airRoute.setAirportFromCity(airports.stream()
//						.filter(ai -> ai.getIata().equals(flight.getDepartureStation()))
//						.findFirst()
//						.map(e -> e.getShortName())
//						.get());
//				airRoute.setAirportToCity(airports.stream()
//						.filter(ai -> ai.getIata().equals(flight.getArrivalStation()))
//						.findFirst()
//						.map(e -> e.getShortName())
//						.get());
//				
//				recordInserted++;
//				//airRouteQueries.addQuery(airRoute);
//				airRouteService.save(airRoute);
//				logger.info("[SAVED] AirRoute is saved!");
//			});
//		});
		//@formatter:on
	}

	private HttpEntity<WizzairRequest> createWizzAirRequest(City airportFrom, String airportTo, Date firstDate) {
		String dateFormatPattern = "yyyy-MM-dd";

		WizzairRequest wizzAirRequest = new WizzairRequest();
		wizzAirRequest.setAdultCount(1);
		wizzAirRequest.setChildCount(0);
		wizzAirRequest.setWdc(true);
		wizzAirRequest.setInfantCount(0);

		FlightList flightList = new FlightList();
		flightList.setDepartureStation(airportFrom.getIata());
		flightList.setArrivalStation(airportTo);
		flightList.setDepartureDate(new SimpleDateFormat(dateFormatPattern).format(firstDate));

		wizzAirRequest.getFlightList().add(flightList);
		HttpEntity<WizzairRequest> request = new HttpEntity<WizzairRequest>(wizzAirRequest);
		return request;
	}

	public List<City> listAirports() {
		RestTemplate rTemplate = new RestTemplate();
		WizzairAirports airports = rTemplate.getForObject(AIRPORT_API, WizzairAirports.class);
		return airports.getCities();
	}

}
