package hu.deniel.mac.module;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ModuleExecutor {

	private static final Logger logger = LoggerFactory.getLogger(ModuleExecutor.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	private static final long SEC = 1000;
	private static final long MIN = 60 * SEC;
	private static final long HOUR = 60 * MIN;

	@Autowired
	@Qualifier("ryanairModule")
	private MacModule ryanairModule;

	@Autowired
	@Qualifier("wizzAirModule")
	private MacModule wizzAirModule;

	@Scheduled(fixedRate = 15 * MIN)
	public void ryanairJob() {
		ryanairModule.runModule();
	}
	
	@Scheduled(fixedRate = 15 * MIN)
	public void wizzAirJob() {
		wizzAirModule.runModule();
	}

}
