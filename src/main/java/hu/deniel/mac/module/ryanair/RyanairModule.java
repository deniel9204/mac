package hu.deniel.mac.module.ryanair;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.pojos.AirRoute;
import hu.deniel.mac.generatedsource.json2pojo.ryanair.Airport;
import hu.deniel.mac.generatedsource.json2pojo.ryanair.Flight;
import hu.deniel.mac.generatedsource.json2pojo.ryanair.RyanairAirlines;
import hu.deniel.mac.generatedsource.json2pojo.ryanair.RyanairAirports;
import hu.deniel.mac.generatedsource.json2pojo.ryanair.Trip;
import hu.deniel.mac.module.MacModule;
import hu.deniel.mac.service.AirRouteService;
import hu.deniel.mac.util.jooq.query.AirRouteQueries;

@Component
public class RyanairModule implements MacModule {

	private static final Logger logger = LoggerFactory.getLogger(RyanairModule.class);

	private static final String AIRPORT_API = "https://api.ryanair.com/aggregate/3/common?embedded=airports";
	private static final String FLIGHT_API = "https://desktopapps.ryanair.com/hu-hu/availability";

	private static final String SOURCE_SYSTEM = "RYANAIR";
	private static final int WEEKS = 30;

	@Autowired
	private AirRouteService airRouteService;

	@Autowired
	private AirRouteQueries airRouteQueries;

	private final List<Airport> airports = listAirports();

	private Integer recordInserted;

	@Override
	public void runModule() {
		logger.info("Ryanair module started...");
		
		// INIT section
		recordInserted = 0;
		StopWatch watchAll = new StopWatch();
		watchAll.start();
		StopWatch watchCurrent = new StopWatch();
		Calendar cal = Calendar.getInstance();

		// RUN section
		for (int i = 0; i < WEEKS; i++) {
			// RUN INIT section
			watchCurrent.start((i+1) + " week process runtime.");
			
			// RUN RUN section
			//@formatter:off
			airports.forEach(airport -> airport.getRoutes()
					.forEach(route -> {
						if (route.split(":")[0].equals("airport")) {
							updateRoutes(airport, airports.stream().filter(e -> e.getIataCode().equals(route.split(":")[1])).findFirst().get(), cal.getTime());
						}
					}));
			//@formatter:on
			
			// RUN POST section
			airRouteService.saveOrUpdateBatch(airRouteQueries);
			airRouteQueries.clearQueries();
			cal.add(Calendar.DAY_OF_MONTH, 7);
			watchCurrent.stop();
			logger.info((i + 1) + " week process ended.");
		}

		// POST section
		logger.info(watchCurrent.prettyPrint());
		watchAll.stop();
		logger.info("Ryanair module ended in " + watchAll.getTotalTimeSeconds() + " second and " + recordInserted
				+ " record processed...");
	}

	private void updateRoutes(Airport airportFrom, Airport airportTo, Date firstDate) {
		try {
			getAirportRoutes(airportFrom, airportTo, firstDate);
		} catch (RestClientException e) {
			logger.error(
					"Cannot extract response from: " + airportFrom.getIataCode() + " to: " + airportTo.getIataCode());
		}
	}

	private void getAirportRoutes(Airport airportFrom, Airport airportTo, Date firstDate) {
		String callUrl = createCallUrl(airportFrom.getIataCode(), airportTo.getIataCode(), firstDate);
		AsyncRestTemplate rTemplate = new AsyncRestTemplate();
		rTemplate.getForEntity(callUrl, RyanairAirlines.class)
				.addCallback(new ListenableFutureCallback<ResponseEntity<RyanairAirlines>>() {
					@Override
					public void onSuccess(ResponseEntity<RyanairAirlines> result) {
						saveAirlines(result.getBody());
					}

					@Override
					public void onFailure(Throwable t) {
						// Need assertions
					}
				});
		callUrl = null;
	}

	private void saveAirlines(final RyanairAirlines airlines) {
		airlines.getTrips().forEach(trip -> {
			trip.getDates().forEach(date -> {
				date.getFlights().forEach(flight -> {
					if(flight.getFaresLeft() != null && flight.getFaresLeft() > 0){
						if (flight.getRegularFare() != null) {
							saveFlight(airlines,trip,date,flight,"REG");
						}
						if (flight.getBusinessFare() != null) {
							saveFlight(airlines,trip,date,flight,"BUS");
						}
						if (flight.getLeisureFare() != null) {
							saveFlight(airlines,trip,date,flight,"LEI");
						}
					}
				});
			});
		});
	}

	private void saveFlight(RyanairAirlines airlines, Trip trip,
			hu.deniel.mac.generatedsource.json2pojo.ryanair.Date date, Flight flight, String fareType) {
		//@formatter:off
		AirRoute airRoute = new AirRoute();
		airRoute.setCostCurrency(airlines.getCurrency());
		airRoute.setAirportFrom(trip.getOrigin());
		airRoute.setAirportFromCity(airports.stream()
				.filter(ai -> ai.getIataCode().equals(trip.getOrigin()))
				.findFirst()
				.map(e -> e.getCityCode())
				.get());
		airRoute.setAirportTo(trip.getDestination());
		airRoute.setAirportToCity(airports.stream()
				.filter(ai -> ai.getIataCode().equals(trip.getDestination()))
				.findFirst()
				.map(e -> e.getCityCode())
				.get());
		airRoute.setRouteNumber(flight.getFlightNumber());
		airRoute.setCostNumBase(flight.getRegularFare()
				.getFares()
				.get(0)
				.getAmount());
		airRoute.setStartTime(new Timestamp(Date.from(Instant.parse(flight.getTimeUTC()
				.get(0)))
				.getTime()));
		airRoute.setArrivalTime(new Timestamp(Date.from(Instant.parse(flight.getTimeUTC()
				.get(1)))
				.getTime()));
		airRoute.setAirRouteRef(flight.getFlightKey());
		airRoute.setSourceSystem(SOURCE_SYSTEM);
		airRoute.setFareType(fareType);
		airRoute.setFaresLeft(flight.getFaresLeft());
		// airRouteService.save(airRoute);
		recordInserted++;
		airRouteQueries.addQuery(airRoute);
		airRoute = null;
		//@formatter:on
	}

	private String createCallUrl(String airportFrom, String airportTo, Date firstDate) {
		String dateFormatPattern = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		sb.append(FLIGHT_API);
		sb.append("?ADT=1");
		sb.append("&CHD=0");
		// sb.append("&DateIn=" + new
		// SimpleDateFormat(dateFormatPattern).format(new Date()));
		sb.append("&DateOut=" + new SimpleDateFormat(dateFormatPattern).format(firstDate));
		sb.append("&Destination=" + airportTo);
		// sb.append("&FlexDaysIn=6");
		sb.append("&FlexDaysOut=6");
		sb.append("&INF=0");
		sb.append("&Origin=" + airportFrom);
		sb.append("&RoundTrip=false");
		sb.append("&TEEN=0");
		return sb.toString();
	}

	private List<Airport> listAirports() {
		RestTemplate rTemplate = new RestTemplate();
		RyanairAirports airports = rTemplate.getForObject(AIRPORT_API, RyanairAirports.class);
		return airports.getAirports();
	}
}
