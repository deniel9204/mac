CREATE TABLE `mac`.`air_route` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `air_route_ref` INT NOT NULL,
  `number` VARCHAR(100) NULL,
  `airport_from` VARCHAR(200),
  `airport_to` VARCHAR(200) NULL,
  `start_time` TIMESTAMP(6) NULL,
  `arrival_time` TIMESTAMP(6) NULL,
  `cost_num` INT NULL,
  `cost_currency` VARCHAR(45) NULL,
  `created_by` VARCHAR(45) NOT NULL,
  `created_time` TIMESTAMP(6) NULL,
  `source_system` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `air_route_ref_UNIQUE` (`air_route_ref` ASC))
ENGINE = InnoDB;