CREATE DATABASE IF NOT EXISTS `mac` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_hungarian_ci */;

GRANT ALL PRIVILEGES ON mac.* TO 'mac_db_conn'@'localhost' IDENTIFIED BY 'password123';