CREATE SEQUENCE "mac"."air_route_id_seq"
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE "mac"."air_route" ( 
	"id" Integer DEFAULT nextval('mac.air_route_id_seq'::regclass) NOT NULL,
	"air_route_ref" Character Varying( 2044 ) COLLATE "pg_catalog"."default" NOT NULL,
	"route_number" Text COLLATE "pg_catalog"."default",
	"airport_from" Text COLLATE "pg_catalog"."default",
	"airport_from_city" Text COLLATE "pg_catalog"."default",
	"airport_to" Text COLLATE "pg_catalog"."default",
	"airport_to_city" Text COLLATE "pg_catalog"."default",
	"start_time" Timestamp With Time Zone,
	"arrival_time" Timestamp With Time Zone,
	"fare_type" Character Varying( 2044 ) not null,
	"fares_left" Integer,
	"club_card" boolean,
	"cost_num_base" double precision,
	"cost_num_discounted" double precision,
	"cost_currency" Character Varying( 2044 ) COLLATE "pg_catalog"."default",
	"created_by" Text COLLATE "pg_catalog"."default" NOT NULL,
	"created_time" Timestamp With Time Zone NOT NULL,
	"last_modified_time" Timestamp With Time Zone NOT NULL,
	"source_system" Character Varying( 2044 ) COLLATE "pg_catalog"."default" NOT NULL,
	PRIMARY KEY ( "id" ) );
 
CREATE UNIQUE INDEX "id_index" ON "mac"."air_route" USING btree( "id" Asc NULLS Last );
ALTER TABLE "mac"."air_route" ADD CONSTRAINT "air_route_ref_fare_type_cc" UNIQUE( "air_route_ref", "fare_type","club_card" );