package hu.deniel.mac.service;

import static hu.deniel.mac.generatedsource.jooq.postgres.db.Tables.AIR_ROUTE;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit4.SpringRunner;

import com.thoughtworks.xstream.XStream;

import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.daos.AirRouteDao;
import hu.deniel.mac.generatedsource.jooq.postgres.db.tables.pojos.AirRoute;
import hu.deniel.mac.service.impl.DefaultAirRouteService;
import hu.deniel.mac.util.spring.MacApplication;
import io.github.benas.randombeans.EnhancedRandomBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { MacApplication.class, DefaultAirRouteService.class, AirRouteDao.class })
public class AirRouteServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(AirRouteServiceTest.class);

	@Autowired
	DSLContext dsl;

	@Autowired
	DataSourceTransactionManager txMgr;

	@Autowired
	AirRouteService airRouteService;

	/**
	 * dec transaction teszt
	 */
	@Test
	@Commit
	public void save_random() {
		boolean assertion = false;
		AirRoute airRoute = EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
				.build()
				.nextObject(AirRoute.class, "id");

		try {
			airRouteService.save(airRoute);
			assertion = true;
		} catch (Exception ignore) {
			logger.error(ignore.getMessage());
			assertion = false;
		}

		Assert.assertTrue(assertion);
	}

	/**
	 * Dao test
	 */
	@Test
	public void findById_first() {
		AirRoute airRoute = airRouteService.findById(1);
		logger.info(new XStream().toXML(airRoute));

		Assert.assertTrue(true);
	}

	/**
	 * Condition and orderby test
	 */
	@Test
	public void list_conditionAndOrderBy() {
		Collection<Condition> conditions = new ArrayList<Condition>();

		conditions.add(AIR_ROUTE.AIRPORT_FROM.contains("dap"));

		List<AirRoute> result = airRouteService.list(conditions, null);

		logger.info(new XStream().toXML(result));
		assertTrue(true);
	}

	/**
	 * Condition and orderby test
	 */
	// @Test
	// public void update_firstRow(){
	//
	// AirRoute airRoute = airRouteService.findById(1);
	// airRoute.setAirportTo("Parizs");
	//
	// airRouteService.saveOrUpdate(airRoute);
	//
	// assertTrue(true);
	// }
}
