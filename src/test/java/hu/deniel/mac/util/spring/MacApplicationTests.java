package hu.deniel.mac.util.spring;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import hu.deniel.mac.util.spring.MacApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MacApplication.class)
public class MacApplicationTests {

	@Test
	public void contextLoads() {
		assertTrue(true);
	}

}
